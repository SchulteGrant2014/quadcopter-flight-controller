//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
////// FLIGHT CONTROLLER for Arduino Uno Quadcopter //////
//////            Author: Grant Schulte             //////
//////            Created: December 2018            //////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

/*
 * ///////////////////////
 * //// Introduction: ////
 * ///////////////////////
 * The following project is a quadcopter flight control software that is designed to run on the Arduino Uno.
 * Quadcopters are inherently unstable and must be tamed by altering the voltage pulse provided to each motor.
 * The flight controller communicates with the sensors (accelerometer and gyroscope) to read inertial data and 
 * use that data to keep the quadcopter in the air.
 * The FLIGHT CONTROLLER has three main responsibilities:
 *    1. Stabilize the quadcopter (by interpreting accelerometer/gyroscope inertial data)
 *    2. Respond to receiver/transmitter signals (to "steer" the quadcopter)
 *    3. Compensate for voltage drop over time (by altering output to the motors)
 * 
 * //////////////////////////////////////
 * //// Note on Learning Objectives: ////
 * //////////////////////////////////////
 * This was a PERSONAL PROJECT: While I could have easily bought a pre-made flight controller, I decided to 
 * build one myself for the experience and for fun and incorporate it into custom-built circuitry.
 * This project allowed me to gain experience in many areas, including:
 *    - Software/Hardware interaction
 *    - Sensor fusion (accelerometer + gyroscope data)
 *    - Circuit design
 *    - Voltage regulation
 *    - Pulse Width Modification
 *    - Soldering, wiring, microcontroller usage
 * 
 * ///////////////////////////////
 * //// Hardware Connections: ////
 * ///////////////////////////////
 * Hardware connections to Arduino Uno: [D = digital pin; A = analog pin]
 *    -- Motors and Electronic Speed Controllers (ESC):
 *        - Right-front (counter-clockwise): D4
 *        - Right-back (clockwise): D5
 *        - Left-back (clockwise): D6
 *        - Left-front (counter-clockwise): D7
 *    -- Receiver:
 *        - Channel 1 (Pitch): D8
 *        - Channel 2 (Roll): D9
 *        - Channel 3 (Throttle): D10
 *        - Channel 4 (Yaw): D11
 *    -- Warning LED:
 *        - LED: D12
 *    -- Gyroscope + Accelerometer (MPU-6050):
 *        - Data: SDA
 *        - Clock: SCL
 *    -- Voltage measurement:
 *        - Battery (+) Terminal (w/ voltage divider): A0
 *    -- Battery Connections to Arduino:
 *        - (+) Terminal: Vin
 *        - (-) Terminal/Ground: GND
 */

#define MICROSECONDS_PER_LOOP 4000;  // Refresh rate of the program - program runs at 250Hz, or once every 4000 microseconds

#include <Wire.h>  // Use SDA pin functionalities to communicate with MPU-6050 Gyroscope+Accelerometer
#include <math.h>

//////////////////////////
//// Global Variables ////
//////////////////////////

// ---------------- Receiver Pulse Data: ----------------

enum receiverChannels {
    channel1,  // 0
    channel2,  // 1
    channel3,  // 2
    channel4   // 3
};
bool previousReceiverPinState[4] = { LOW, LOW, LOW, LOW };  // [0] = Channel 1; [1] = Channel 2; [2] = Channel 3; [3] = Channel 4
unsigned long receiverRisingEdgeTime[4] = { 0, 0, 0, 0 };  // Time at which the voltage to the ESC rose from 0V to 5V, causing a Pin Change Interrupt
int receiverPulseLength[4] = { 0, 0, 0, 0 };  // Pulse length obtained from each receiver channel... { pitch, roll, throttle, yaw }


// ---------------- PID Controller Pulse Length Calculation Data: ----------------

unsigned long loopEndTime = 0;  // The time at which the program cycle should roll over to the next cycle in order to maintain a 250Hz refresh rate
unsigned long currentTime = 0;  // The time that is used to determine when a loop cycle should end (at loopEndTime). Also used to determine loopEndTime value at start of loop

int batteryVoltage = 0;  // Voltage held by the battery

enum ESCs {
    ESC1,  // Right-front
    ESC2,  // Right-back
    ESC3,  // Left-back
    ESC4  // Left-front
};
unsigned long pulseStartTime_ESC = 0;  // Time at which the voltage pulse to each ESC starts
unsigned long pulseEndTime_ESC[4] = { 0, 0, 0, 0 };  // Time at which the voltage pulse to each ESC should end (defined as pulseStartTime + pulseLength)
int correctedPulseLength[4] = { 0, 0, 0, 0 };  // Length of the voltage pulse to be sent to each ESC, after applying PID corrections

float pidGain_p[3] =  { 1.3,   1.3,   4.0 };  // { roll_pGain, pitch_pGain, yaw_pGain }
float pidGain_i[3] =  { 0.04,  0.04,  0.02 };  // { roll_iGain, pitch_iGain, yaw_iGain }
float pidGain_d[3] =  { 18.0,  18.0,  0.0 };  // { roll_dGain, pitch_dGain, yaw_dGain }
int pidMaxOutput[3] = { 400,   400,   400 };  // { roll_maxOutput, pitch_maxOutput, yaw_maxOutput }

float pidError[3] = { 0, 0, 0 };  // { roll_error, pitch_error, yaw_error }
float pidErrorPrevious[3] = { 0, 0, 0 };  // { roll_prevError, pitch_prevError, yaw_prevError }
float pidController_p[3] = { 0, 0, 0 };  // { roll_p, pitch_p, yaw_p }
float pidController_i[3] = { 0, 0, 0 };  // { roll_i, pitch_i, yaw_i }
float pidController_d[3] = { 0, 0, 0 };  // { roll_d, pitch_d, yaw_d }

float pidOutput[3] = { 0, 0, 0 };  // { roll_output, pitch_output, yaw_output }

float transmitterSensitivity = 3.0;  // Higher value = less sensitive


// ---------------- MPU-6050 Gyroscope/Accelerometer Data: ----------------

enum axes {
    roll,   // 0
    pitch,  // 1
    yaw     // 2
};
const short gyroAddress = 0x68;
const short gyro_firstDataRegister = 0x3B;
short gyroOutput[3] = { 0, 0, 0 };  // Raw values output from the gyroscope { x_angularVelocity, y_angularVelocity, z_angularVelocity }, corrected with calibration values
short accelOutput[3] = { 0, 0, 0 };  // Raw values output from the accelerometer { x_acceleration, y_acceleration, z_acceleration }
short gyroOutputOffset[3] = { 0, 0, 0 };  // Store calibration values - upon startup, how far are gyro readings off from zero? { roll, pitch yaw }
float accelAngleOffset[2] = { -0.5, -0.4 };  // Offset of the accelerometer reading (pre-defined from testing accelerometer angle on a known-flat surface) { roll, pitch }
float angleGyro[2] = { 0, 0 };  // ANGULAR POSITION as determined by continuous integration of gyroscope values, with accelerometer drift correction { roll, pitch }
float angleGyro_change[3] = { 0, 0, 0 };  // Change in Angular Position over one 250Hz cycle as determined by integration of gyro data { roll_dx, pitch_dx, yaw_dx }
float angleAccel[2] = { 0, 0 };  // ANGULAR POSITION as determined by accelerometer values (only) { roll, pitch }
float angularVelocityGyro[3] = { 0, 0, 0 };  // { roll_dw, pitch_dw, yaw_dw }
short temperature;

bool motorOn = false;  // Determines whether the quadcopter is running or not


/////////////////////////////////////////
//// Main Flight Controller Routines ////
/////////////////////////////////////////

void setup() {
    
    // Configure pins as input/output pins (input by default)
    DDRD |= B11110000;       // Set digital pins 4, 5, 6 and 7 as output.
    DDRB |= B00110000;       // Set digital pins 12 and 13 as output.

    // Prepare to receive gyro/accel data
    // Begin I2C communication with the MPU-6050 Gyroscope/Accelerometer and set registers to desired settings
    bool success = setupMPU_6050();

    // If MPU-6050 setup failed, it is unsafe to fly the quadcopter - turn on warning LED and arrest program execution
    if (success == false) {
        digitalWrite(12, HIGH);    // Turn on warning LED
        while (true) {             // Halt program execution until Arduino Uno is reset, preventing flight
            delay(1000);           // Delay 1 second (1000 milliseconds) to prevent such a fast loop
        }
    }

    // Calibrate the gyro to negate any measured movement when the gyro is actually stationary. LED will blink at 0.5 second intervals
    calibrateGyro();

    // Set digital pins 8, 9, 10, and 11 to trigger a Pin Change Interrupt upon state change
    PCICR |= (1 << PCIE0);            //Set PCIE0 to enable PCMSK0 scan.
    PCMSK0 |= (1 << PCINT0);          //Set PCINT0 (digital input 8) to trigger an interrupt on state change.
    PCMSK0 |= (1 << PCINT1);          //Set PCINT1 (digital input 9) to trigger an interrupt on state change.
    PCMSK0 |= (1 << PCINT2);          //Set PCINT2 (digital input 10) to trigger an interrupt on state change.
    PCMSK0 |= (1 << PCINT3);          //Set PCINT3 (digital input 11) to trigger an interrupt on state change.

    // Wait for the user to set the throttle to the lowest position to start the quadcopter, blinking LED to indicate ready
    waitForMotorOnSignal();
    
    // Start the loop timer to track when the loop() should end to maintain a 250Hz refresh rate (one cycle = 4000 microseconds, cycle should end when time expires)
    currentTime = micros();
    
}



void loop() {

    if (motorOn) {
        
        // ---------------- Set the end time for the loop (4000 microseconds [250Hz]) ----------------
        loopEndTime = currentTime + MICROSECONDS_PER_LOOP;
        
        // ---------------- Determine angular position + angular velocity from MPU-6050 data ----------------
        readMPU_raw();
        calculateAngularPosition();
        calculateAngularVelocity();
        
        // ---------------- Calculate PID corrections to apply to each ESC / motor ----------------
        calculatePIDCorrections();
        
        // ---------------- Read battery for compensating for voltage drop over time ----------------
        readBatteryVoltage();
        
        // ---------------- Calculate the voltage pulse length to send to each ESC / motor ----------------
        computeESCPulseLength();
        
        // ---------------- Calculations are done: Send calculated voltage pulses to each ESC / motor ----------------
        sendVoltagePulseToESC();
        
        // ---------------- Check for signal to stop motors (when throttle down + right) ----------------
        checkForMotorOffSignal();
        
        // ---------------- Finish the program cycle only after 4000 milliseconds total time is elapsed, maintaining a 250Hz refresh rate ----------------
        
        // Wait until the 4000 microsecond loop is finished...
        currentTime = micros();
        while (loopEndTime > currentTime) {
            currentTime = micros();
        }
        
        // If program loop ran for more than 4050 milliseconds, the 250Hz refresh rate is no longer accurate - stabilization faulty!
        if ( (loopEndTime - currentTime) > 4050) {
            PORTB |= B00010000;  // So... turn on warning LED to inform the user that the craft is unsafe to fly!
        }
        
    } else {
        
        // Check for signal to turn motors on
        waitForMotorOnSignal();
        currentTime = micros();
        
    }
    
}



/*
 * Waits until the receiver shows THROTTLE DOWN (channel3) and YAW LEFT (channel4) to turn on motors and exit the function.
 */
void waitForMotorOnSignal() {
  
    while (receiverPulseLength[channel3] > 1020 || receiverPulseLength[channel4] > 1020) {
        digitalWrite(12, !digitalRead(12));
        delayMicroseconds(500);  // Blink LED on/off every 0.5 seconds to indicate that quadcopter is ON
    }
    motorOn = true;
}



/*
 * Checks for THROTTLE DOWN (channel3) and YAW RIGHT (channel4) to turn off motors.
 */
void checkForMotorOffSignal() {
    
    if (receiverPulseLength[channel3] < 1020 && receiverPulseLength[channel4] > 1980) {
        motorOn = false;
    }
}



/*
 * Sets up the MPU-6050 Gyroscope + Accelerometer by first establishing an I2C connection through the DTA and CLK pins.
 * It then sets the MPU-6050's registers to the desired values to wake the gyro and dial in the optimal settings for this quadcopter.
 * Finally, a safety check is made to ensure that all registers were in fact successfully set to the appropriate values.
 * @return True if registers set successfully, False if registers set incorrectly.
 */
bool setupMPU_6050() {

    // Register addresses
    short PWR_MGMT_1 = 0x6B;      // This register sleeps (1) or wakes (0) the gyro
    short GYRO_CONFIG = 0x1B;     // This register sets the range of the gyroscope output
    short ACCEL_CONFIG = 0x1C;    // This register sets the range of the accelerometer output
    short CONFIG = 0x1A;          // This register sets the Digital Low Pass Filter frequency

    // Begin I2C communication
    Wire.begin();  // Start the I2C communication to communicate with MPU-6050 Gyroscope/Accelerometer
    TWBR = 12;     // Set the I2C clock speed to be 400kHz

    // Enable the gyro (by default, gyro is in "sleep" mode)
    byte wakeSignal = B00000000;
    Wire.beginTransmission(gyroAddress);    // Begin communication with the MPU-6050 Gyro/Accel
    Wire.write(PWR_MGMT_1);                 // Contact the PWR_MGMT_1 register so we can enable the gyro+accel
    Wire.write(wakeSignal);                 // Set PWR_MGMT_1 register to B00000000 wake the gyro
    Wire.endTransmission();                 // Stop communicating with PWR_MGMT_1 register 
    
    // Set range of gyroscope output
    byte gyroConfigSignal = B00001000;
    Wire.beginTransmission(gyroAddress);    // Begin communication with the MPU-6050 Gyro/Accel
    Wire.write(GYRO_CONFIG);                // Contact the GYRO_CONFIG register so we can set gyroscope output range
    Wire.write(gyroConfigSignal);           // Set GYRO_CONFIG register to a gyro output range of +/-500 degrees/sec
    Wire.endTransmission();                 // Stop communicating with GYRO_CONFIG register

    // Set the range of accelerometer output
    byte accelConfigSignal = B00010000;
    Wire.beginTransmission(gyroAddress);    // Begin communication with the MPU-6050 Gyro/Accel
    Wire.write(ACCEL_CONFIG);               // Contact the ACCEL_CONFIG register so we can set accelerometer output range
    Wire.write(accelConfigSignal);          // Set GYRO_CONFIG register to a gyro output range of +/-8g
    Wire.endTransmission();                 // Stop communicating with ACCEL_CONFIG register

    // Set the Digital Low Pass Filter (DLPF) frequency
    byte dlpfSignal = B00000011;
    Wire.beginTransmission(gyroAddress);    // Begin communication with the MPU-6050 Gyro/Accel
    Wire.write(CONFIG);                     // Contact the CONFIG register so we can set Digital Low Pass Filter frequency
    Wire.write(dlpfSignal);                 // Set CONFIG register to Digital Low Pass Filter frequency ~43Hz (~4.85ms delay)
    Wire.endTransmission();                 // Stop communicating with ACCEL_CONFIG register
    
    // SAFETY: Read all registers to check that the gyroscope was set up correctly... if not, return false
    if (!testGyroRegister(PWR_MGMT_1, wakeSignal) || !testGyroRegister(GYRO_CONFIG, gyroConfigSignal) || !testGyroRegister(ACCEL_CONFIG, accelConfigSignal) || !testGyroRegister(CONFIG, dlpfSignal)) {
        return false;
    }
    
    return true;  // If all registers set successfully
    
}



/*
 * Tests for whether an MPU-6050 register (registerAddress) contains the expected value (signalSent).
 * If the value of the address (registerAddress) matches the expected value (signalSent), then this function returns true, else false.
 * @param registerAddress The address of the register to be checked
 * @param signalSent The expected value of the register
 * @return true if register has expected value, else false.
 */
bool testGyroRegister(short registerAddress, byte signalSent) {
    Wire.beginTransmission(gyroAddress);
    Wire.write(registerAddress);
    Wire.endTransmission();
    Wire.requestFrom(gyroAddress, 1, true);
    while (Wire.available() < 1) {
        // wait until all bytes (1) are received
    }
    byte safetyTest = Wire.read();
    if (safetyTest == signalSent) {
        return true;
    } else {
        return false;
    }
}



/*
 * Reads the MPU-6050 Gyroscope and Accelerometer over the I2C bus to obtain the =RAW= roll, pitch, and yaw values 
 * of angular velocity as well as values of acceleration.
 * NOTE: Because this function simply reads the raw data, the units must be converted from MPU-6050 arbitrary units
 * to degrees or degrees/second in order to be usable.
 */
void readMPU_raw() {
    
    // Request gyroscope, temperature, and accelerometer data from the MPU-6050
    Wire.beginTransmission(gyroAddress);
    Wire.write(gyro_firstDataRegister);    // First data register contains AccelerationX High Byte
    Wire.endTransmission();
    Wire.requestFrom(gyroAddress, 14);     // Request all 14 bytes of data from MPU-6050, auto-increment through all data registers
    while (Wire.available() < 14) {
        // wait until all data is transferred
    }

    // Store new data from MPU-6050
    accelOutput[roll] = (Wire.read() << 8) | Wire.read();     // X-axis. First read high bit and put into place, then read low bit. Combine.
    accelOutput[pitch] = (Wire.read() << 8) | Wire.read();    // Y-axis
    accelOutput[yaw] = (Wire.read() << 8) | Wire.read();      // Z-axis
    temperature = (Wire.read() << 8) | Wire.read();
    gyroOutput[roll] = (Wire.read() << 8) | Wire.read();      // X-axis
    gyroOutput[pitch] = (Wire.read() << 8) | Wire.read();     // Y-axis
    gyroOutput[yaw] = (Wire.read() << 8) | Wire.read();       // Z-axis
    
    // Apply gyroscope calibration (note: during calibration, the offset values are 0).
    gyroOutput[roll] -= gyroOutputOffset[roll];
    gyroOutput[pitch] -= gyroOutputOffset[pitch];
    gyroOutput[yaw] -= gyroOutputOffset[yaw];
    
    /* // Apply temperature conversion
     * temperature = (temperature / float(340)) + 36.53    // As defined in the MPU-6050 manual
     */
    
}



/*
 * Calculates the angular position of the quadcopter using measurements from both the gyroscope and accelerometer.
 * A complementary filter is applied to obtain angular position: the gyroscope values determine angular position 
 * while the accelerometer values adjust for gyroscope drift.
 */
void calculateAngularPosition() {
    
    // First, calculate the angle according to the gyroscope

    // Determine change in axis-specific angles over time, and add that change to the additive angle value
    angleGyro_change[roll] = (gyroOutput[roll] * 0.00006107);     // angularPosition = [gyroOutput gyro units] * [(1 deg/sec) / 65.5 gyro units)] * [(1/250) seconds] = degrees
    angleGyro_change[pitch] = (gyroOutput[pitch] * 0.00006107);   // angularPosition = [gyroOutput gyro units] * [(1 deg/sec) / 65.5 gyro units)] * [(1/250) seconds] = degrees
    angleGyro_change[yaw] = gyroOutput[yaw] * 0.00006107;         // angularPosition = [gyroOutput gyro units] * [(1 deg/sec) / 65.5 gyro units)] * [(1/250) seconds] = degrees
    
    // Couple roll to pitch through the yaw axis to account for environment-referenced roll and pitch changes upon yawing
    angleGyro_change[roll] += (angleGyro[pitch] + angleGyro_change[pitch]) * sin(angleGyro_change[yaw] * (PI / 180.0));    // Convert yaw angle to radians for use in sin() function
    angleGyro_change[pitch] -= (angleGyro[roll] + angleGyro_change[roll]) * sin(angleGyro_change[yaw] * (PI / 180.0));    // Convert yaw angle to radians for use in sin() function

    // Next, calculate the angle according to the accelerometer
    
    // Calculate the total acceleration vector from the accelerometer angles
    float totalAccelerationVector = sqrt( (angleAccel[roll]*angleAccel[roll]) + (angleAccel[pitch]*angleAccel[pitch]) + (angleAccel[yaw]*angleAccel[yaw]) );

    // Calculate the pitch and roll angles from this acceleration vector (assumes total acceleration vector always points toward ground)
    if (abs(accelOutput[roll]) < totalAccelerationVector) {  // Prevent division by 0 and eventual 
        angleAccel[roll] = asin( accelOutput[roll] / totalAccelerationVector ) * -(180 / PI);
    }
    if (abs(accelOutput[pitch]) < totalAccelerationVector) {  // Prevent division by 0
        angleAccel[pitch] = asin( accelOutput[pitch] / totalAccelerationVector ) * (180 / PI);
    }

    // Correct the angles calculated from the accelerometer using the pre-defined calibration values
    angleAccel[roll] -= accelAngleOffset[roll];
    angleAccel[pitch] -= accelAngleOffset[pitch];

    // Finally, use a complementary filter to combine angles calculated from both gyroscope and accelerometer data:
    // Gyro for angle measurement, accel for drift correction
    angleGyro[roll] = 0.9996 * (angleGyro[roll] + angleGyro_change[roll]) + 0.0004 * (angleAccel[roll]);
    angleGyro[pitch] = 0.9996 * (angleGyro[pitch] + angleGyro_change[pitch]) + 0.0004 * (angleAccel[pitch]);
    
}


/*
 * Calculates the angular velocity of the quadcopter by converting gyroscope readings (units: "gyro units") to degrees per second.
 * At gyroscope configuration of +/-500degrees per second full range, ANGULAR VELOCITY: 65.5 gyro units = 1 degree per second.
 */
void calculateAngularVelocity() {

    // Convert "gyro units" to "degrees per second", applying complementary filter to reduce noise
    float degreesPerSecond = gyroOutput[roll] / 65.5;  // [gyro units] * [ 1 dps / 65.5 gyro units] = degrees per second
    angularVelocityGyro[roll] = (0.7 * angularVelocityGyro[roll]) + (0.3 * degreesPerSecond);  // Remove noise with complementary filter

    degreesPerSecond = gyroOutput[pitch] / 65.5;
    angularVelocityGyro[pitch] = (0.7 * angularVelocityGyro[pitch]) + (0.3 * degreesPerSecond);  // Complementary filter
    
    degreesPerSecond = gyroOutput[yaw] / 65.5;
    angularVelocityGyro[yaw] = (0.7 * angularVelocityGyro[yaw]) + (0.3 * degreesPerSecond);  // Complementary filter
    
}



/*
 * Calibrates the gyro by taking 2000 readings and averaging them to determine the amount of offset from zero.
 * These offset values are placed into the gyro offset array.
 * Assumes that the gyro is not moving. Automatically corrects for uneven surface by initially setting quadcopter angles to accel values.
 * @param The array into which the offset values will be placed
 */
void calibrateGyro() {

    bool LED_status = LOW;  // Use blinking LED to indicate that gyro calibration is in progress
    
    // Take 2000 gyro measurements (20 seconds), storing the sum of the measurements
    long roll_sum = 0;
    long pitch_sum = 0;
    long yaw_sum = 0;
    
    for (int i = 0; i < 2000; ++i) {
      
        readMPU_raw();  // Obtain updated gyroscope data
        roll_sum += gyroOutput[roll];
        pitch_sum += gyroOutput[pitch];
        yaw_sum += gyroOutput[yaw];
        delay(4);  // Pause 4 milliseconds to let the gyro refresh

        if (i % 20 == 0) {          // Blink LED quickly to indicate that calibration is in progress
            if (LED_status == HIGH) {
                PORTB &= B11101111;  // Turn off LED (digital pin 12)
            } else {
                PORTB |= B00010000;  // Turn on LED (digital pin 12)
            }
        }
    }

    // Take average of all readings to get the average offset/calibration value for each axis
    gyroOutputOffset[roll] = roll_sum / 2000;
    gyroOutputOffset[pitch] = pitch_sum / 2000;
    gyroOutputOffset[yaw] = yaw_sum / 2000;

    // Set the initial angle of the quadcopter based on the accelerometer (senses gravitational force vector to identify "down" direction)
    readMPU_raw();    // Read the raw values from the gyroscope + accelerometer
    calculateAngularPosition();    // Calculate the angle of the quadcopter from accelerometer data
    angleGyro[roll] = angleAccel[roll];    // Set roll of quad using ONLY the accelerometer data, as the accelerometer references gravity for orientation
    angleGyro[pitch] = angleAccel[pitch];    // Set pitch of quad using ONLY the accelerometer data, as the accelerometer references gravity for orientation
    // Note: Yaw cannot be accurately represented by an accelerometer.
    
    // Turn off LED to indicate that calibration has finished
    PORTB &= B11101111;
    
}



/*
 * Pin Change Interrupt subroutine that is automatically called whenever digital pins 8, 9, 10, or 11 change state.
 * Specifically, the rising or falling edge of a receiver pulse (from any of channels 1-4) triggers this subroutine.
 * @param PCINT0_vect The vector revealing which pin (of the enabled PCI pins) caused an interrupt
 */
ISR(PCINT0_vect) {
  
    unsigned long timeOfInterrupt = micros();                     // Determine the current time in microseconds
    
    // Check which pins changed and compute the pulse length...
    // ================ Channel 1 ================
    if (PINB & B00000001) {                                       // If digital pin 8 is HIGH...
        if (previousReceiverPinState[channel1] == LOW) {          // And its state was changed (from LOW)...
            receiverRisingEdgeTime[channel1] = timeOfInterrupt;   // Then the pulse is starting, so save the rising edge timestamp...
            previousReceiverPinState[channel1] = HIGH;            // And note that the pin is now HIGH
        }
    } else if (previousReceiverPinState[channel1] == HIGH) {      // Otherwise, if pin 8 is LOW and its state changed (from HIGH)...
        receiverPulseLength[channel1] = timeOfInterrupt - receiverRisingEdgeTime[channel1];  // Then the pulse has ended, so calculate pulse length!
        previousReceiverPinState[channel1] = LOW;
    }
    // ================ Channel 2 ================
    if (PINB & B00000010) {                                       // If digital pin 9 is HIGH...
        if (previousReceiverPinState[channel2] == LOW) {          // And its state was changed (from LOW)...
            receiverRisingEdgeTime[channel2] = timeOfInterrupt;   // Then the pulse is starting, so save the rising edge timestamp...
            previousReceiverPinState[channel2] = HIGH;            // And note that the pin is now HIGH
        }
    } else if (previousReceiverPinState[channel2] == HIGH) {      // Otherwise, if pin 9 is LOW and its state changed (from HIGH)...
        receiverPulseLength[channel2] = timeOfInterrupt - receiverRisingEdgeTime[channel2];  // Then the pulse has ended, so calculate pulse length!
        previousReceiverPinState[channel2] = LOW;
    }
    // ================ Channel 3 ================
    if (PINB & B00000100) {                                       // If digital pin 10 is HIGH...
        if (previousReceiverPinState[channel3] == LOW) {          // And its state was changed (from LOW)...
            receiverRisingEdgeTime[channel3] = timeOfInterrupt;   // Then the pulse is starting, so save the rising edge timestamp...
            previousReceiverPinState[channel3] = HIGH;            // And note that the pin is now HIGH
        }
    } else if (previousReceiverPinState[channel3] == HIGH) {      // Otherwise, if pin 10 is LOW and its state changed (from HIGH)...
        receiverPulseLength[channel3] = timeOfInterrupt - receiverRisingEdgeTime[channel3];  // Then the pulse has ended, so calculate pulse length!
        previousReceiverPinState[channel3] = LOW;
    }
    // ================ Channel 4 ================
    if (PINB & B00001000) {                                       // If digital pin 11 is HIGH...
        if (previousReceiverPinState[channel4] == LOW) {          // And its state was changed (from LOW)...
            receiverRisingEdgeTime[channel4] = timeOfInterrupt;   // Then the pulse is starting, so save the rising edge timestamp...
            previousReceiverPinState[channel4] = HIGH;            // And note that the pin is now HIGH
        }
    } else if (previousReceiverPinState[channel4] == HIGH) {      // Otherwise, if pin 11 is LOW and its state changed (from HIGH)...
        receiverPulseLength[channel4] = timeOfInterrupt - receiverRisingEdgeTime[channel4];  // Then the pulse has ended, so calculate pulse length!
        previousReceiverPinState[channel4] = LOW;
    }
    
}



/*
 * Corrects the voltage pulse length from the receiver to stabilize the quadcopter.
 * Uses a PID controller to stabilize the quadcopter - the receiver tells the quadcopter to fly with a particular angular velocity,
 * but the quadcopter is unstable and therefore may inadvertently obtain less or extra angular velocity... in this case, the voltage
 * pulse sent to each motor is corrected by the PID controller to ensure an accurate angular velocity is obtained.
 */
void calculatePIDCorrections() {
    
    // ---------------- Calculate PID setpoint - i.e. the angular velocity that the receiver WANTS the quad to fly with ----------------
    
    float pidSetpoint[3] = { 0, 0, 0 };   // 0 degrees per second, i.e. stationary by default... { roll_setpoint, pitch_setpoint, yaw_setpoint }

    /* Translate receiver pulse into an angular velocity that the receiver wants the quad to fly at.
     * 1492 to 1508 pulse range is reserved for stationary flight (i.e. don't want the quad to rotate)
     * Outside the stationary range, the residual pulse length above or below this range is set as the angular velocity (i.e. if 1600 pulse length, then 1600-1508 = 92dps)
     * This angular velocity is scaled by a sensitivity factor so the transmitter sticks aren't too "touchy" - with sensitivity = 3.0, max angular velocity = 500/3 = 166.6dps
     */
     
    // Calculate angular velocity setpoint for all axes
    
    if (receiverPulseLength[channel1] > 1508) {
        pidSetpoint[roll] = (receiverPulseLength[channel1] - 1508);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else if (receiverPulseLength[channel1] < 1492) {
        pidSetpoint[roll] = (receiverPulseLength[channel1] - 1492);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else {
        pidSetpoint[roll] = 0;  // In stationary zone, the quadcopter should not move
    }

    if (receiverPulseLength[channel2] > 1508) {
        pidSetpoint[pitch] = (receiverPulseLength[channel2] - 1508);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else if (receiverPulseLength[channel2] < 1492) {
        pidSetpoint[pitch] = (receiverPulseLength[channel2] - 1492);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else {
        pidSetpoint[pitch] = 0;  // In stationary zone, the quadcopter should not move
    }

    if (receiverPulseLength[channel4] > 1508) {
        pidSetpoint[yaw] = (receiverPulseLength[channel4] - 1508);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else if (receiverPulseLength[channel1] < 1492) {
        pidSetpoint[yaw] = (receiverPulseLength[channel4] - 1492);  // Reduce number of "degrees per second" per receiver pulse unit based on sensitivity value
    } else {
        pidSetpoint[yaw] = 0;  // In stationary zone, the quadcopter should not move
    }

    // Make the quadcopter stop rotating once it hits the desired angle (i.e. if transmitter sticks are in middle, quad stops rotating once it is level, 0 degrees)
    // Also, apply a sensitivity factor to increase/decrease how far the quadcopter rotates for a given stick movement
    
    pidSetpoint[roll] -= angleGyro[roll] * 15;       // Correct the angle correction to keep the quadcopter stationary when the desired angle is reached
    pidSetpoint[roll] /= transmitterSensitivity;     // Decrease the angle that the quadcopter achieves for each unit of pulse length
    pidSetpoint[pitch] -= angleGyro[pitch] * 15;     // Correct the angle correction to keep the quadcopter stationary when the desired angle is reached
    pidSetpoint[pitch] /= transmitterSensitivity;    // Decrease the angle that the quadcopter achieves for each unit of pulse length
    // Note: No angular position calculated for yaw because we want the quadcopter to keep yawing (without stopping at a setpoint) the whole time the transmitter stick is out of the center position
    pidSetpoint[yaw] /= transmitterSensitivity;      // Decrease the angle that the quadcopter achieves for each unit of pulse length
    
    // ---------------- Calculate the PID-corrected angular velocity output ----------------
    
    for (int axis = 0; axis < 3; ++axis) {
        
        // Calculate PID error - i.e. deviation of actual angular velocity from desired (setpoint)
        pidError[axis] = angularVelocityGyro[axis] - pidSetpoint[axis];
        
        // P-controller (proportional)
        pidController_p[axis] = pidGain_p[axis] * pidError[axis];
        
        // I-controller (integral - additive)
        pidController_i[axis] = pidController_i[axis] + (pidGain_i[axis] * pidError[axis]);
        if (pidController_i[axis] > pidMaxOutput[axis]) {
            pidController_i[axis] = pidMaxOutput[axis];
        } else if (pidController_i[axis] < (pidMaxOutput[axis] * -1)) {
            pidController_i[axis] = pidMaxOutput[axis] * -1;
        }
        
        // D-controller (derivative - considers change from previous error value)
        pidController_d[axis] = pidGain_d[axis] * (pidError[axis] - pidErrorPrevious[axis]);

        // !!!!!!!! Calculate PID angular velocity output !!!!!!!!
        
        pidOutput[axis] = pidController_p[axis] + pidController_i[axis] + pidController_d[axis];
        
        if (pidOutput[axis] > pidMaxOutput[axis]) {
            pidOutput[axis] = pidMaxOutput[axis];
        } else if (pidOutput[axis] < (pidMaxOutput[axis] * -1)) {
            pidOutput[axis] = pidMaxOutput[axis] * -1;
        }
        
        // Set PID previous error to the current error, preparing for next PID correction function call
        pidErrorPrevious[axis] = pidError[axis];
        
    }
    
}



/*
 * Reads the voltage of the battery through the analog input pin 0. Voltage is determined by the value of the analog pin. 
 * An analog pin takes on values of 0 to 1023 (1023 units per 5V).
 * The voltage drop over the resistor converts a full 12.6V battery to ~5V. Analog pin 0 reads 1023 at 5V.
 * (1260 value) * (1 Volt / 1023 units) = 1.23167 Volt Values / unit  ==>  batteryVoltage will read 1260V at 5V
 * If the voltage is too low, the warning LED is turned on to indicate low battery.
 */
void readBatteryVoltage() {
    
    // Compute battery voltage from analog pin 0
    batteryVoltage = (analogRead(0) + 65) * 1.23167;  // See function description
    
    // Reduce noise from voltage readings using a complementary filter
    batteryVoltage = (0.92 * batteryVoltage) + (0.08 * (analogRead(0) + 65) * 1.23167);
    
    // Turn on warning LED if voltage is too low to indicate low battery
    if(batteryVoltage < 1000 && batteryVoltage > 600) {
        digitalWrite(12, HIGH);
    }
    
}



/*
 * Computes the pulse length to send to each ESC given the desired pulse length (specified by the receiver) and corrections to
 * the desired pulse length (specified by the PID calculation).
 */
void computeESCPulseLength() {
    
    int throttle = receiverPulseLength[channel3];
    
    // Set hard limit on throttle to maintain full control of quadcopter
    if (throttle > 1800) {
        throttle = 1800;
    }
    
    // Compute the pulse length to give to each motor based on PID corrections
    correctedPulseLength[ESC1] = throttle + pidOutput[roll] - pidOutput[pitch] - pidOutput[yaw];  // Front-right motor (Counter-Clockwise rotation)
    correctedPulseLength[ESC2] = throttle + pidOutput[roll] + pidOutput[pitch] + pidOutput[yaw];  // Back-right motor (Clockwise rotation)
    correctedPulseLength[ESC3] = throttle - pidOutput[roll] + pidOutput[pitch] - pidOutput[yaw];  // Back-left motor (Counter-Clockwise rotation)
    correctedPulseLength[ESC4] = throttle - pidOutput[roll] - pidOutput[pitch] + pidOutput[yaw];  // Front-left motor (Clockwise rotation)
    
    // Further correct ESC voltage pulse lengths based on battery voltage
    for (int i = 0; i < 4; ++i) {
        correctedPulseLength[i] += correctedPulseLength[i] * ((1240 - batteryVoltage)/(float)3500);
    }

    // Keep motor pulses strictly between 1100 and 2000 microseconds
    for (int i = 0; i < 4; ++i) {
        if (correctedPulseLength[i] < 1100) {
            correctedPulseLength[i] = 1100;
        } else if (correctedPulseLength[i] > 2000) {
            correctedPulseLength[i] = 2000;
        }
    }
    
}



/*
 * Sends a voltage pulse to each motor. The length of each pulse is determined by correctedPulseLength, whose value is assumed to have been previously calculated
 * through taking receiver output pulse lengths and subjecting them to PID corrections for quadcopter stabilization.
 */
void sendVoltagePulseToESC() {
  
    pulseStartTime_ESC = micros();  // Get start time of ESC voltage pulse
    PORTD = B11110000;  // Start the voltage pulse to the ESCs - set digital pins 4, 5, 6, and 7 HIGH
    
    // Determine when each pin should be set LOW to end the voltage pulse to the ESC
    pulseEndTime_ESC[ESC1] = pulseStartTime_ESC + correctedPulseLength[ESC1];  // Voltage pulse to ESC1 should last "correctedPulseLength[ESC1]" microseconds
    pulseEndTime_ESC[ESC2] = pulseStartTime_ESC + correctedPulseLength[ESC2];  // Voltage pulse to ESC2 should last "correctedPulseLength[ESC2]" microseconds
    pulseEndTime_ESC[ESC3] = pulseStartTime_ESC + correctedPulseLength[ESC3];  // Voltage pulse to ESC3 should last "correctedPulseLength[ESC3]" microseconds
    pulseEndTime_ESC[ESC4] = pulseStartTime_ESC + correctedPulseLength[ESC4];  // Voltage pulse to ESC4 should last "correctedPulseLength[ESC4]" microseconds

    // Constantly monitor the time and set each ESC pin LOW when its pulse timer expires
    while ((PORTD & B11110000) == B00000000) {  // Keep looping until all pins (digital pins 4, 5, 6, and 7) are set LOW, indicating that all pulses have finished
        currentTime = micros();
        if ( pulseEndTime_ESC[ESC1] <= currentTime ) { PORTD &= B11101111; }  // If channel1 pulse timer expired, set digital pin 4 LOW
        if ( pulseEndTime_ESC[ESC2] <= currentTime ) { PORTD &= B11011111; }  // If channel2 pulse timer expired, set digital pin 5 LOW
        if ( pulseEndTime_ESC[ESC3] <= currentTime ) { PORTD &= B10111111; }  // If channel3 pulse timer expired, set digital pin 6 LOW
        if ( pulseEndTime_ESC[ESC4] <= currentTime ) { PORTD &= B01111111; }  // If channel4 pulse timer expired, set digital pin 7 LOW
    }
    
}
 



