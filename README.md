
## **I. Introduction:**

The following project is a quadcopter flight control software that is designed to run on the Arduino Uno.
Quadcopters are inherently unstable and must be tamed by altering the voltage pulse provided to each motor.
The flight controller communicates with the sensors (accelerometer and gyroscope) to read inertial data and
use that data to keep the quadcopter in the air.

The FLIGHT CONTROLLER has three main responsibilities:
   1. Stabilize the quadcopter (by interpreting accelerometer/gyroscope inertial data)
   2. Respond to receiver/transmitter signals (to "steer" the quadcopter)
   3. Compensate for voltage drop over time (by altering output to the motors)

## **II. Note on Learning Objectives:**

This was a PERSONAL PROJECT: While I could have easily bought a pre-made flight controller, I decided to
build one myself for the experience and for fun and incorporate it into custom-built circuitry.

This project allowed me to gain experience in many areas, including:
   - Software/Hardware interaction
   - Sensor fusion (accelerometer + gyroscope data)
   - Circuit design
   - Voltage regulation
   - Pulse Width Modification
   - Soldering, wiring, microcontroller usage

## **III. Hardware Connections:**

Hardware components were connected to Arduino pins as shown below: [D = digital pin; A = analog pin]
   - Motors and Electronic Speed Controllers (ESC):
       - Right-front (counter-clockwise): D4
       - Right-back (clockwise): D5
       - Left-back (clockwise): D6
       - Left-front (counter-clockwise): D7
   - Receiver:
       - Channel 1 (Pitch): D8
       - Channel 2 (Roll): D9
       - Channel 3 (Throttle): D10
       - Channel 4 (Yaw): D11
   - Warning LED:
       - LED: D12
   - Gyroscope + Accelerometer (MPU-6050):
       - Data: SDA
       - Clock: SCL
   - Voltage measurement:
       - Battery (+) Terminal (w/ voltage divider): A0
   - Battery Connections to Arduino:
       - (+) Terminal: Vin
       - (-) Terminal/Ground: GND

## **IV. Arduino Uno Pin/Port Details:**

In this project, direct port manipulation is used to interract with the individual pins of the Arduino Uno.

While using the Arduino, individual pins are read from or written to in order to interract with attached devices. There are two ways
to interract with individual pins. The first method is to utilize the methods `pinMode()`, `digitalRead()`, and `digitalWrite()`.
However, these methods are slow and consume much more memory. The alternative to using these methods to access individual
pins is to directly manipulate the ports. Direct port manipulation is faster and consumes less memory, so it is the preferred method
for a quadcopter flight controller, which must run quickly and with high temporal accuracy. Specifically, there must be enough time
in each cycle to both calculate the length of the pulse to be sent to each motor and to actually send the pulse to the ESC/motor.

_Benefits of direct port manipulation (versus built-in methods):_
  - Faster setting of pins
  - Can set multiple pins at a time
  - Less memory usage
_Drawbacks of port manipulation (versus built-in methods):_
  - Harder to read the code for debugging
  - Less portable (ports/pins are specific to each Ardiuno microcontroller type)
  - Potential to cause malfunctions (pins 0 and 1 are serial receive/transmit)

Overview of **direct** port manipulation of the Ardiuno Uno (ATmega328P Microcontroller):
   - The ATmega328P has 3 ports (arrays of pins):
       - B (Digital Pins 8-13)
       - C (Analog Pins 0-5)
       - D (Digital Pins 0-7)
   - Each port is controlled by 3 different registers (bytes), whose bits control a corresponding pin in that port:
       - DDR register = denotes each pin as INPUT or OUTPUT [read/write]
       - PORT register = sets each pin as HIGH or LOW voltage [read/write]
       - PIN register = reads the state of pins set as INPUT pins [read ONLY]
   - Access these registers to alter pin settings:
       - ex 1.) `PORTD = B10101000;` // sets digital pins 7,5,3 HIGH (5V)
       - ex 2.) `DDRD = B11111110;`  // sets Arduino pins 1 to 7 as outputs, pin 0 as input
       - ex 3.) `DDRD |= B11111100;`  // this is safer as it sets pins 2 to 7 as outputs without changing the
                                                                     // value of pins 0 and 1, which are for serial communications

**Built-in** Port Manipulation Methods (alternative to direct port manipulation):
   - `pinMode(pinNumber, mode) -> void`
       - Sets the pin type to either INPUT (`false`) or OUTPUT (`true`)
   - `digitalRead(pinNumber) -> bool`
       - Reads the voltage applied to the specified pin
       - Returns HIGH (`true`) or LOW (`false`)
   - `digitalWrite(pinNumber, value) -> void`
       - Sets the pin voltage to either HIGH [5V, `true`] or LOW [0V, `false`]

## **V. Pin Change Interrupts (PCI):**

Because quadcopters are inherently very unstable, the flight controller must make corrections at very high frequency.
This flight controller is configured to run with a refresh rate of 250Hz. This means that the quadcopter's tilt is
corrected every 4000 microseconds. However, the refresh rate of the receiver is much lower: 62.5Hz. This means that
the receiver input can only be changed every 16,000 microseconds. The program must continue to run at 250Hz to keep
the quadcopter stable, so it cannot wait for new receiver input data to become available. It must keep running using
old values and update once new values become available.
==> Solution: Pin Change Interrupts (PCI)

Using Pin Change Interrupts, the program can "cut away" from its normal execution when new receiver data becomes
available to handle that data, then return to normal execution. In the case of this flight controller, the receiver
pulses are measured using a PCI system to determine the thrust that the user wants to apply to each of the motors.
When the pulse shoots high or dips low, the value of the pin (HIGH/LOW) is changed. This triggers the PCI routine.
The time in between the pulse shooting high and the pulse lowering is the width of the wave in microseconds. This
time width describes the amount of thrust that the user wants to apply to a particular set of motors.

***A. Quick overview:***
   - PCI# = Pin Change Interrupt control vector number "#"
       - A control vector that contains the routine that runs when a pin state change (Pin Change Interrupt) is detected.
   - PCICR = Pin Change Interrupt Control Register
       - Used to enable/disable a Pin Change Interrupt sequence entirely (the method that runs when a PCI is triggered).
       - A register whose bits specify whether an entire Pin Change Interrupt routine is enabled (1) or disabled (0).
       - Its bits are PCIE0, PCIE1, and PCIE2, which enable/disable Pin Change Interrupt routines PCI0/PCI1/PCI2 entirely.
   - PCMSK# = Pin Change Mask Register number "#"
       - Used to enable/disable specific
       - A register whose bits specify which individual pins are enabled/disabled to trigger a PCI# routine.
       - Its bits are PCINT0 through PCINT7. These bits enable/disable monitoring of digital pins 8-11 for state changes.

***B. Detailed overview:***

Pin changes (HIGH[5V]<-->LOW[0V]) trigger interrupt sequences. Each port (B,C,D) has an associated interrupt sequence:
   - Port B (Digital Pins 8-13): PCI0
   - Port C (Analog Pins 0-5): PCI1
   - Port D (Digital Pins 0-7): PCI2
   
Whether or not the Pin Change Interrupt (PCI) sequences trigger is determined by the bits of the Pin Change Interrupt
Control Register (PCICR). When a bit is HIGH, that PCI# sequence is enabled for its corresponding pins and will trigger
in the event of a pin change. 3 bits of the register correspond to the enabled/disabled state of the 3 PCI sequences:
   - PCIE0 --> Bit 0 of PCICR  // Enables the PCI0 sequence to trigger when digital pins 8-13 are changed
   - PCIE1 --> Bit 1 of PCICR  // Enables the PCI1 sequence to trigger when alanog pins A0-A5 are changed
   - PCIE2 --> Bit 2 of PCICR  // Enables the PCI2 sequence to trigger when digital pins 8-13 are changed
   
When a pin change occurs, the PCI# sequence is triggered if PCIE# is set to "enabled" (1). For example, the PCI sequence
"PCI0" will trigger if any of the digital pins 8-11 (connected to the receiver) change state. But it is also possible to
set which of these pins (8-11) should cause an interrupt and which should not. Each PCI# sequence is associated with a
mask, PCMSK#. For example, PCI0 is associated with PCMSK0. The bits of PCMSK0 determine which pins are enabled to cause
an interrupt upon state change:
   - ex: PCINT0 --> Bit 0 of PCMSK0 determines whether the digital pins 8 will trigger a PCI routine
   - Note: See ATmega328P schematic to see which pins of each mask register enable which pins to cause PCI routines
   
Enabling Pin Change Interrupts to occur is done as follows... Specifically, we are enabling the PCI0 control vector and 4 pins:
  - `PCICR |= (1 << PCIE0);`     //Set PCIE0 to enable PCMSK0 scan.
  - `PCMSK0 |= (1 << PCINT0);`   //Set PCINT0 (digital input 8) to trigger an interrupt on state change.
  - `PCMSK0 |= (1 << PCINT1);`   //Set PCINT1 (digital input 9)to trigger an interrupt on state change.
  - `PCMSK0 |= (1 << PCINT2);`   //Set PCINT2 (digital input 10)to trigger an interrupt on state change.
  - `PCMSK0 |= (1 << PCINT3);`   //Set PCINT3 (digital input 11)to trigger an interrupt on state change.

## **VI. I2C Bus - Gyroscope Readings:**

The MPU-6050 Gyroscope and Accelerometer was used in this project. The MPU-6050 uses a standard I2C bus for data
transmission. The I2C bus synchronizes data transfer using its SCL line (clock line). It uses its SDA line (data line) for data transfer.
The standard clock (SCL) speed for I2C is up to 100KHz. The I2C bus has many "slave" devices which can be read/written to in
this way.

Data is transferred in a sequence of 8 bits, starting with the most-significant bit. The SCL (clock) line is pulsed HIGH for each bit
transferred over the SDA (data) line. These pulses indicate that a bit is currently being transferred over the SDA line, whether the
data bit is HIGH or LOW. The first 7 bits are the data transferred, and the last bit is an acknowledgement bit sent back from the slave.

The I2C address of the MPU-6050 is 7 bits. The address is sent over the lines as 8 bits, where the last bit is the "read/write" bit,
indicating whether data is being read or written from/to the slave (0 = write, 1 = read).

**I2C Write Protocol:**
1. Send a start sequence [to begin the transmission]
2. Send the I2C address of the slave (read/write bit = LOW) [to specify which slave to access]
3. Send the specific register number that you want to write to [to specify which register of the slave to access]
4. Send the data byte (8 bits) [to write this data to the register of the slave]
5. _Note:_ At this point, any further data bytes sent will be written to subsequent registers - i.e. the internal register address of the slave will auto-increment with each byte sent.
6. Send the STOP sequence [to end the transmission]

**I2C Read Protocol:**
1. Send a START sequence [to begin the transmission]
2. Send the I2C address of the device (read/write bit = LOW) [to specify which device to access]
3. Send the internal register address [to specify which internal address one wants to read]
4. Send a start sequence (again) [to indicate that a new operation is beginning]
5. Send the I2C address of the device (read/write bit = HIGH) [to specify which device to access]
6. Read data byte from the register [to get the data from the specified register]
7. Send the STOP sequence [to end the transmission]

**MPU-6050 I2C Register Addresses:** (from manual)
* MPU-6050 Device: `0x68`
* PWR_MGMT_1 Register: `0x6B` --> Enable/disable gyroscope+accelerometer
* GYRO_CONFIG Register: `0x1B` --> Set full range of gyroscope output
* ACCEL_CONFIG Register: `0x1C` --> Set full range of accelerometer output
* CONFIG Register: `0x1A` --> Set digital low pass filter frequency
* Acceleration[X] High Byte (Bits 8-15): `0x3B`
* Acceleration[X] Low Byte (Bits 0-7): `0x3C`
* Acceleration[Y] High Byte (Bits 8-15): `0x3D`
* Acceleration[Y] Low Byte (Bits 0-7): `0x3E`
* Acceleration[Z] High Byte (Bits 8-15): `0x3F`
* Acceleration[Z] Low Byte (Bits 0-7): `0x40`
* Temperature High Byte (Bits 8-15): `0x41`
* Temperature High Byte (Bits 0-7): `0x42`
* Gyroscope[X] High Byte (Bits 8-15): `0x43`
* Gyroscope[X] Low Byte (Bits 0-7): `0x44`
* Gyroscope[Y] High Byte (Bits 8-15): `0x45`
* Gyroscope[Y] Low Byte (Bits 0-7): `0x46`
* Gyroscope[Z] High Byte (Bits 8-15): `0x47`
* Gyroscope[Z] Low Byte (Bits 0-7): `0x48`

**Technical Notes on Registers:**
PWR_MGMT_1:
* Controls whether the gyroscope+accelerometer are enabled or not (specifically bit 6)
* B00000000 = sleep/disabled
* B11111111 = wake/enabled
GYRO_CONFIG:
* Bits 3-4 (B000XX000) control the full scale range of gyroscope outputs
* B00000000 = +/- 250 deg/sec      ==> 131 data units = 1 degree/second
* B00001000 = +/- 500 deg/sec      ==> 65.5 data units = 1 degree/second
* B00010000 = +/- 1000 deg/sec    ==> 32.8 data units = 1 degree/second
* B00011000 = +/- 2000 deg/sec    ==> 16.4 data units = 1 degree/second
ACCEL_CONFIG:
* Bits 3-4 (B000XX000) control the full scale range of accelerometer outputs
* B00000000 = +/- 2g
* B00001000 = +/- 4g
* B00010000 = +/- 8g
* B00011000 = +/- 16g
CONFIG:
* Bits 0-2 control the frequency of the Digital Low Pass Filter applied to the gyro+accel
* B00000000 = ~258Hz @ 0.00 accel delay, 0.98ms gyro delay
* B00000001 = ~186Hz @ ~1.95ms delay
* B00000010 = ~96Hz @ ~2.9ms delay
* B00000011 = ~43Hz @ ~4.85ms delay
* B00000100 = ~21.5Hz @ ~8.4ms delay
* B00000101 = ~10Hz @ ~13.6ms delay
* B00000110 = ~5Hz @ ~18.8ms delay

**Interpreting the MPU-6050 Outputs**
The MPU-6050 gyroscope and accelerometer outputs data values that must be converted to utilize the data. The exact conversion
factors depend on the full scale range of the gyroscope and accelerometer (See GYRO_CONFIG and ACCEL_CONFIG registers).

_Reading Gyroscope Angular Velocity Data:_
- Full Scale Range: _+/- 250 deg/sec_
    - Conversion: 131 data units = 1 degree/second
- Full Scale Range: _+/- 500 deg/sec_
    - Conversion: 65.5 data units = 1 degree/second
- Full Scale Range: _+/- 1000 deg/sec_
    - 32.8 data units = 1 degree/second
- Full Scale Range: _+/- 2000 deg/sec_
    - 16.4 data units = 1 degree/second

_Reading Accelerometer Acceleration Data:_
- Full Scale Range: _+/- 2g_
    - Conversion: 16,384 data units = 1g (gravitational force)
- Full Scale Range: _+/- 4g_
    - Conversion: 8,192 data units = 1g (gravitational force)
- Full Scale Range: _+/- 8g_
- Conversion: 4,096 data units = 1g (gravitational force)
- Full Scale Range: _+/- 16g
    - Conversion: 2,048 data units = 1g (gravitational force)

_Reading Temperature Data:_
- Temperature(Celsius) = (Temperature reading from MPU-6050) / 340 + 36.53

Note that the gyroscope output (angular velocity) can be converted to angular position by integration:
This flight controller operates on a 250Hz loop (250 loops / second). Additionally, the full scale range of the gyroscope is set to
+/- 500 degrees/sec, meaning that 65.5 data units = 1 degree/second. Therefore,  angular velocity/position is computed as follows:
`angularVelocity = [gyroOutput data units] * [(1 deg/sec) / 65.5 data units)] = degrees / second`
`angularPosition = [gyroOutput data units] * [(1 deg/sec) / 65.5 data units)] * [(1/250) seconds] = degrees`

Concretely:
`angularPosition = gyroOutput / 0.00006107`

Also note, when the quadcopter is angled (not flat), any movement on the yaw axis will cause the quadcopter to roll and pitch in
reference to the environment, but these movements will not be recorded because this yawing is isolated to the yaw axis from the
gyroscope's perspective. Therefore, the roll and pitch axes should be coupled together through the yaw axis:
`pitchAngle = pitchAngle - rollAngle * sin(yawAngle)`
`rollAngle = rollAngle + pitchAngle * sin(yawAngle)`

Finally, the accelerometer can be used to correct the drift in gyroscope values that occurs over time:
`pitchAngle_accel = asin( accelOutputPitch / totalAccelerationVector )`
`rollAngle_accel = -asin( accelOutputRoll / totalAccelerationVector )`

Combine the two angle measurements using the complementary filter approach to obtain a driftless angle. Utilize the gyro reading
to compute the angular position and correct for gyro drift by using a small portion of accelerometer values:
`pitchAngle = (pitchAngle * 0.9996) + (pitchAngle_accel * 0.0004);`
`rollAngle = (rollAngle * 0.9996) + (rollAngle_accel * 0.0004);`








